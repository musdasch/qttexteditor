#include "./ui/editor.h"
#include <QApplication>

int main(int argc, char **argv) {
	QApplication a(argc, argv);
	Editor window;

	window.setWindowTitle("Editor");
	window.show();

	return a.exec();
}
