#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTextEdit>

namespace Ui {
class Editor;
}

class Editor : public QMainWindow
{
    Q_OBJECT

public:
    explicit Editor(QWidget *parent = 0);
    ~Editor();

private slots:
	void on_actionOpen_triggered();
	void on_actionSave_triggered();
	void on_actionExit_triggered();
	void on_actionTest_triggered();

private:
    Ui::Editor *ui;
    QTextEdit *textEdit;

    QString openFileDialog(QString title);
    void readFile(QString fileName);
    void writeFile(QString fileName);
};

#endif
