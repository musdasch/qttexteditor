#include "editor.h"
#include "ui_editor.h"
#include <iostream>

#include <QFileDialog>
#include <QFile>
#include <QTextStream>

Editor::Editor(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Editor)
{
	ui->setupUi(this);
	textEdit = ui->textEdit;
}

Editor::~Editor()
{
    delete ui;
}

void Editor::on_actionOpen_triggered() {
	QString fileName = openFileDialog("Open File...");
	readFile(fileName);
}

void Editor::on_actionSave_triggered() {
	QString fileName = openFileDialog("Save as...");
	writeFile(fileName);
}

void Editor::on_actionExit_triggered() {
	this->close();
}

void Editor::on_actionTest_triggered() {
	QString text = textEdit->toPlainText();
	std::cout << "Text:" << std::endl << text.toStdString() << std::endl;
}

QString Editor::openFileDialog(QString title) {
	QFileDialog fileDialog(this, title);

	fileDialog.setAcceptMode(QFileDialog::AcceptOpen);
	fileDialog.setFileMode(QFileDialog::ExistingFile);
	fileDialog.setMimeTypeFilters({"text/plain"});

	if (fileDialog.exec() != QDialog::Accepted)
		return "";

	return fileDialog.selectedFiles().constFirst();
}

void Editor::readFile(QString fileName) {
	QFile file(fileName);

	if (file.open(QIODevice::ReadOnly | QIODevice::Text)) {
		QTextStream stream(&file);

		textEdit->clear();
		textEdit->moveCursor(QTextCursor::End);

		while (!stream.atEnd()) {
			QString text = stream.read(1);
			textEdit->insertPlainText(text);
		}

		textEdit->moveCursor(QTextCursor::Start);
	}

	file.close();
}

void Editor::writeFile(QString fileName) {
	QFile file(fileName);

	if (file.open(QIODevice::WriteOnly | QIODevice::Text)) {
		QString text = textEdit->toPlainText();
		QTextStream stream( &file );

		stream << text;
	}

	file.close();
}
